class ApiConstants {
  static const String baseUrl = "https://staging.blkem.com/mobile_api/";
  static const String loginEndPoint = "login";
  static const String signUpEndPoint = "signup";
  static const String resetPassword = "resetpassword";
  static const String postFeeds = "feeds";
}
